# SentinelC experiments app library

This app library is used for beta/alpha quality app integrations.

Use apps in this repository at your own risks. No quality guarantee.

As apps become stable and generally useful, they can be promoted to the official repository.


