This project comes as a pre-built docker image that enables you to easily forward to your websites running at home or otherwise, including free SSL, without having to know too much about Nginx or Letsencrypt.

## SentinelC notes

The admin interface is reachable using the built-in cloud proxy. Just click Link to web based interface.

To self-host services, you will need:

- Configure port forwards for port 80 and 443 pointing to nginx proxy manager.

For each exposed service:

- Setup a domain name pointing to your public IP address.
- Install the sentinelc service in the same zone as nginx proxy manager.
- Configure the Host under nginx proxy manager, pointing to the private IP address assigned to your service.  