## Jellyfin

An empty volume named 'media' will be created on install.

Use the 'filebrowser' app or similar to upload your media.

Jellyfin web UI will be available remotely through the cloud proxy and locally on the assigned IP address in the zone you install the service, using the default port 8096.

